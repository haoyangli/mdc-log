package com.nb.log;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ThreadPoolExecutor;


@SpringBootApplication
public class NbLogApplication {


    public static void main(String[] args) {

        SpringApplication.run(NbLogApplication.class, args);

    }



}
