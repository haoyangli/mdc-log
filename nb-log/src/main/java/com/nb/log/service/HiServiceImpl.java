package com.nb.log.service;

import com.nb.log.NbLogApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author lihaoyang
 * @date 2021/3/17
 */
@Service
public class HiServiceImpl {
    static Logger logger = LoggerFactory.getLogger(NbLogApplication.class);

    public String getHi(String msg) {
        return "service:  >>>>> " + msg;
    }

    public String postHi(String msg) {
        return "service:  >>>>> " + msg;
    }
}

