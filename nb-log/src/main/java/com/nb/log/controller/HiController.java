package com.nb.log.controller;

import com.nb.log.NbLogApplication;
import com.nb.log.service.HiServiceImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lihaoyang
 * @date 2021/3/17
 */
@RestController
public class HiController {
    static Logger logger = LoggerFactory.getLogger(NbLogApplication.class);

    @Autowired
    HiServiceImpl hiService;

    @GetMapping("get")
    public String get(String msg) {
        String rs = hiService.getHi(msg);
        return rs;
    }

    @GetMapping("get2")
    public String get2(String msg) {
        String rs = hiService.getHi(msg);
        return rs;
    }
}
